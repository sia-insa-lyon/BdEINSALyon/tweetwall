# Demande de fonctionnalités

<!--
  Nous vous remercions de contribuer à ce projet par la création de cette issue !
  Pour éviter les duplicats, nous vous demandons de cocher les cases dans la liste qui suit.
-->

<!-- Cocher une case doit ressembler à ceci : [x] -->

- [ ] Cette issue est présente dans la dernière version livrée.
- [ ] J'ai cherché dans les [issues](https://gitlab.com/sia-insa-lyon/BdEINSALyon/tweetwall/issues) de ce répertoire et crois que ceci n'est pas un duplicat.

## Résumé 💡

<!-- Décrivrez comment cela devrait fonctionner. -->

## Exemples 🌈

<!--
  Fournisez un lien vers des ressources, user story, ou des captures d'écran du comportement attendu.
-->

## Motivation 🔦

<!--
  Qu'essayez vous d'accomplir ? Comment le manque de cette fonctionnalité vous a affecté ?
  En donnant du contexte, vous nous aidez à réaliser une solution qui sera la plus utile possible dans le monde réel.
-->

## Actions rapides
<!---Ne touchez pas à cette section sauf si vous savez ce que vous faites. Ces actions rapides sont là pour améliorer le traitement des issues par la suite.--->
/label Feature
/label "Awaiting Feedback"
