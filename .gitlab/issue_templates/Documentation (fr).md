# Issue concernant la documentation

<!--
  Nous vous remercions de contribuer à ce projet par la création de cette issue !
  Pour éviter les duplicats, nous vous demandons de cocher les cases dans la liste qui suit.
-->

<!-- Cocher une case doit ressembler à ceci : [x] -->

- [ ] Cette issue concerne la dernière version mise en ligne.
- [ ] J'ai cherché dans les [issues](https://gitlab.com/sia-insa-lyon/BdEINSALyon/tweetwall/issues) de ce répertoire et crois que ceci n'est pas un duplicat.

## Besoin 💡

<!-- Décrivrez ce qu'il vous manque dans la documentation sur ce projet, ce qu'il faudrait ajouter. -->

## Exemples 🌈

<!--
  Fournisez un lien vers des ressources pour nous aider à documenter.
-->

## Motivation 🔦

<!--
  Qu'essayez vous d'accomplir ? Comment le manque dans la documentation vous a affecté ?
  En donnant du contexte, vous nous aidez à réaliser une solution qui sera la plus utile possible dans le monde réel.
-->

## Actions rapides
<!---Ne touchez pas à cette section sauf si vous savez ce que vous faites. Ces actions rapides sont là pour améliorer le traitement des issues par la suite.--->
/label Documentation
/label "Awaiting Feedback"
