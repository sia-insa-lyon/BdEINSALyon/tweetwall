# Basic issue

<!--
  Thank you for contributing to this project by creating an issue!
  To avoid duplicate issues we ask you to check off the following list.

  Before you start writing this issue, please check that the available issue template don't correspond to your kind of issue.
  If there is any template corresponding, please change the "Basic issue" title to a more explicit title and feel free to add sections.
-->

<!-- Checked checkbox should look like this: [x] -->

- [ ] The issue is present in the latest release.
- [ ] I have searched the [issues](https://gitlab.com/sia-insa-lyon/BdEINSALyon/tweetwall/issues) of this repository and believe that this is not a duplicate.

## Context 🔦

<!--
  What are you trying to accomplish? How has this issue affected you?
  Providing context helps us come up with a solution that is most useful in the real world.
-->

## (Optional) Your Environment 🌎

<!--
  OPTIONAL - Only for technical issue (for example: concerning CI/CD, code documentation/style/linter ...).
  IMPORTANT - You can erase this section and its content if you don"t want to complete it.
  Include as many relevant details about the environment with which you experienced the issue.
  Please complete the following table if this issue is technical.
-->
| Env infos        | Version |
| ----------- | ------- |
| Browser name     |         |
| On Smartphone  | :no_entry_sign: or :heavy_check_mark: |

## Quick actions
<!---Don't touch this section unless you know what you do. Theses quick actions are here to enhance the treatment of issues afterwards.--->

/label "Awaiting Feedback"
