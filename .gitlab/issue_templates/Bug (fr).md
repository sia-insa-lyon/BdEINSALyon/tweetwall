# Découverte de bug

<!--
  Nous vous remercions de contribuer à ce projet par la création de cette issue !
  Pour éviter les duplicats, nous vous demandons de cocher les cases dans la liste qui suit.
-->

<!-- Cocher une case doit ressembler à ceci : [x] -->

- [ ] Cette issue est présente dans la dernière version livrée.
- [ ] J'ai cherché dans les [issues](https://gitlab.com/sia-insa-lyon/BdEINSALyon/tweetwall/issues) de ce répertoire et crois que ceci n'est pas un duplicat.

## Comportement actuel 😯

<!-- Décrivez ce qui arrive et qui différe du comportement attendu. -->

## Comportement attendu 🤔

<!-- Décrivez ce qui devrait arriver. -->

## Etapes pour reproduire 🕹

<!--
Donnez un lien vers un exemple disponible et un ensemble d'étapes claires à suivre pour reproduire le bug.
Les issues sans exemples disponibles auront un temps de réponse/résolution plus long de la part de l'équipe.
-->

Etapes à reproduire:

1.
1.
1.
1.

## (Optionnel) Contexte 🔦

<!--
  Qu'essayez vous d'accomplir ? Comment ce bug vous a affecté ?
  En donnant du contexte, vous nous aidez à réaliser une solution qui sera la plus utile possible dans le monde réel.
-->

## Your Environment 🌎

<!--
  Ajouter autant de détails que vous juge utile sur l'environnement avec lequel vous avez eu le bug.
  S'il vous plait, complétez la table qui suit si cette issue relève du domaine technique.
-->
| Env infos        | Version |
| ----------- | ------- |
| Nom du navigateur     |         |
| Sur un smartphone  | :no_entry_sign: or :heavy_check_mark: |

## Actions rapides
<!---Ne touchez pas à cette section sauf si vous savez ce que vous faites. Ces actions rapides sont là pour améliorer le traitement des issues par la suite.--->
/label Priority::High
/label Bug::Discovery
