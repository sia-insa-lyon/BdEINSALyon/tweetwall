# Documentation issue

<!--
  Thank you for contributing to this project by creating an issue!
  To avoid duplicate issues we ask you to check off the following list.
-->

<!-- Checked checkbox should look like this: [x] -->

- [ ] The issue is present in the latest release.
- [ ] I have searched the [issues](https://gitlab.com/sia-insa-lyon/BdEINSALyon/tweetwall/issues) of this repository and believe that this is not a duplicate.

## Summary 💡

<!-- Describe how the documentation should change. -->

## Examples 🌈

<!--
  Provide a link to some resources, user story, or screenshots to help us solve this issue.
-->

## Motivation 🔦

<!--
  What are you trying to accomplish? How has the lack of documentation affected you?
  Providing context helps us come up with a solution that is most useful in the real world.
-->

## Quick actions
<!---Don't touch this section unless you know what you do. Theses quick actions are here to enhance the treatment of issues afterwards.--->
/label Documentation
/label "Awaiting Feedback"
